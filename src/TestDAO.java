import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @(#) TestDAO.java
 */


public class TestDAO {

    public static boolean insertTest (Test test) {
        Connection con = null;
        Statement stmt = null;
        int result = 0;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeUpdate("INSERT INTO TEST VALUES (" + test.toString() + ")");
            con.commit();
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return result != 0;
    }

    public static boolean testExists(String patientID, LocalDate date) {
        Connection con = null;
        Statement stmt = null;
        ResultSet result = null;
        boolean output = false;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection(
                    "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeQuery(
                    "SELECT * FROM TEST WHERE FOLDERID = '" + patientID + "' and DATE = '" + date + "'");
            while (result.next()) {
                output = true;
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return output;
    }

    public void getTests(String patientID) {

    }


}
