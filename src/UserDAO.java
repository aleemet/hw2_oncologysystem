import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @(#) UserDAO.java
 */


public class UserDAO {

    public static boolean userWithNameExists(String userName) {
        Connection con = null;
        Statement stmt = null;
        ResultSet result = null;
        boolean output = false;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection(
                    "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeQuery(
                    "SELECT * FROM USER WHERE USERNAME = '" + userName + "'");
            while (result.next()) {
                output = true;
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return output;
    }

    public static boolean insertUser(User user) {
        Connection con = null;
        Statement stmt = null;
        int result = 0;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeUpdate("INSERT INTO USER VALUES (" + user.toString() + ")");
            con.commit();
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return result != 0;
    }

    public static List<User> userExists(String userName, String password) {
        Connection con = null;
        Statement stmt = null;
        ResultSet result = null;
        List<User> users = new ArrayList<>();
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection(
                    "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeQuery(
                    "SELECT * FROM USER WHERE USERNAME like '%" + userName + "%'");
            while (result.next()) {
                User user = new User();
                user.setUsername(result.getString("username"));
                user.setPassword(result.getString("password"));
                user.setRole(result.getString("role"));
                users.add(user);
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return users;
    }
}
