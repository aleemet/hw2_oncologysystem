import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * @(#) MedicineDAO.java
 */


public class MedicineDAO {
    public static boolean medicineWithNameExists(String name) {
        Connection con = null;
        Statement stmt = null;
        ResultSet result = null;
        boolean output = false;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection(
                    "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeQuery(
                    "SELECT * FROM MEDICINE WHERE NAME = '"+name+"'");
            while(result.next()){
                output= true;
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return output;
    }

    public static boolean insertMedicine(Medicine medicine) {
        Connection con = null;
        Statement stmt = null;
        int result = 0;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeUpdate("INSERT INTO MEDICINE VALUES ("+medicine.toString()+")");
            con.commit();
        }catch (Exception e) {
            e.printStackTrace(System.out);
        }
        if(result == 0){
            return false;
        }
        else{
            return true;
        }
    }


    public static List<Medicine> staffExists(String name, String company, String code) {
        Connection con = null;
        Statement stmt = null;
        ResultSet result = null;
        List<Medicine> meds = new ArrayList<>();
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection(
                    "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeQuery(
                    "SELECT * FROM MEDICINE WHERE name = '%"+name+"%'");
            while(result.next()){
                Medicine med = new Medicine();
                med.setName(result.getString("name"));
                med.setMedicineCode(result.getString("code"));
                med.setMedicineCompany(result.getString("company"));
                meds.add(med);
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return meds;
    }

    public static List<Medicine> getMeds(String name){
        Connection con = null;
        Statement stmt = null;
        ResultSet result = null;
        List<Medicine> meds = new ArrayList<>();
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection(
                    "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeQuery(
                    "SELECT * FROM MEDICINE WHERE NAME like '%" + name + "%' ");
            while (result.next()) {
                Medicine med = new Medicine();
                med.setMedicineCode(result.getString("code"));
                med.setName(result.getString("name"));
                med.setMedicineCompany(result.getString("company"));
                meds.add(med);
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return meds;
    }


}
