import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @(#) Patient.java
 */


public class Patient {
    private String surname;

    private String name;

    private String IDcode;

    private LocalDate dob;

    private InsuranceType insuranceType;

    private String insuranceCode;

    private String insuranceCompany;

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIDcode() {
        return IDcode;
    }

    public void setIDcode(String IDcode) {
        this.IDcode = IDcode;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public InsuranceType getInsuranceType() {
        return insuranceType;
    }

    public void setInsuranceType(InsuranceType insuranceType) {
        this.insuranceType = insuranceType;
    }

    public String getInsuranceCode() {
        return insuranceCode;
    }

    public void setInsuranceCode(String insuranceCode) {
        this.insuranceCode = insuranceCode;
    }

    public String getInsuranceCompany() {
        return insuranceCompany;
    }

    public void setInsuranceCompany(String insuranceCompany) {
        this.insuranceCompany = insuranceCompany;
    }

    @Override
    public String toString() {
        return String.format("'%s', '%s', '%s', '%s', '%s', '%s', '%s'",
                IDcode, name, surname, dob, insuranceType.toString(), insuranceCode, insuranceCompany);
    }
}
