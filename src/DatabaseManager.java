import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class DatabaseManager {
    public static void initializeDatabase() {
        Connection con = null;
        Statement stmt = null;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            stmt.executeUpdate(
                    "CREATE TABLE medicalstaff (" +
                    "professionalID varchar(20) NOT NULL, " +
                    "name varchar (30), " +
                            "surname varchar (30), " +
                    "occupation varchar (15) , " +
                            "type varchar (15), " +
                    "PRIMARY KEY (professionalID));");
            stmt.executeUpdate(
                    "CREATE TABLE patient (" +
                            "IDcode varchar(20) NOT NULL, " +
                            "name varchar (30), " +
                            "surname varchar (30), " +
                            "dob date , " +
                            "insuranceType varchar (8), " +
                            "insuranceCode varchar (50), " +
                            "insuranceCompany varchar (20), " +
                            "PRIMARY KEY (IDcode));");
            stmt.executeUpdate(
                    "CREATE TABLE user (" +
                            "username varchar(30) NOT NULL, " +
                            "password varchar (30), " +
                            "role varchar (20), " +
                            "PRIMARY KEY (username));");
            stmt.executeUpdate(
                    "CREATE TABLE medicine (" +
                            "code varchar(20) NOT NULL, " +
                            "name varchar (30), " +
                            "company varchar (30), " +
                            "PRIMARY KEY (code));");
            stmt.executeUpdate(
                    "CREATE TABLE hospitalTherapySpot (" +
                            "id INTEGER IDENTITY, " +
                            "startDate date NOT NULL, " +
                            "endDate date NOT NULL, " +
                            "PRIMARY KEY (id));");
            stmt.executeUpdate(
                    "CREATE TABLE folder (" +
                            "patientID varchar(20) NOT NULL, " +
                            "follower varchar (20) NOT NULL , " +
                            "testHistory varchar (30), " +
                            "anamnesis varchar (30), " +
                            "PRIMARY KEY (patientID), " +
                            "FOREIGN KEY (patientID) references patient(IDcode), " +
                            "FOREIGN KEY (follower) references medicalstaff(professionalID));");
            stmt.executeUpdate(
                    "CREATE TABLE visit   (" +
                            "folderID varchar(20) NOT NULL, " +
                            "date date, report varchar (30), " +
                            "type varchar (20), " +
                            "decision varchar (30), " +
                            "PRIMARY KEY (folderID, date), " +
                            "FOREIGN KEY (folderID) REFERENCES folder(patientID));");
            stmt.executeUpdate(
                    "CREATE TABLE test    (" +
                            "folderID varchar(20) NOT NULL, " +
                            "date date, " +
                            "type varchar (20), " +
                            "PRIMARY KEY (folderID), " +
                            "FOREIGN KEY (folderID) references folder(patientID));");
            stmt.executeUpdate(
                    "CREATE TABLE therapy (" +
                            "folderID varchar(20) NOT NULL, " +
                            "startDate date, endDate date, " +
                            "type varchar(20), " +
                            "medicine varchar(20), " +
                            "posology varchar(20), " +
                            "therapySpot integer, " +
                            "PRIMARY KEY (folderID, startDate, endDate), " +
                            "FOREIGN KEY (folderID) references folder(patientID), " +
                            "FOREIGN KEY (medicine) references medicine(code), " +
                            "FOREIGN KEY (therapySpot) references hospitalTherapySpot(id));");
            stmt.executeUpdate(
                    "CREATE TABLE surgery (" +
                            "folderID varchar(20) NOT NULL, " +
                            "date date, " +
                            "surgeon varchar(20), " +
                            "team varchar(20), " +
                            "PRIMARY KEY (folderID, date), " +
                            "FOREIGN KEY (folderID) references folder(patientID));");
           // stmt.executeUpdate("CREATE TABLE enrollment (studentID varchar(20) NOT NULL, title varchar(20) NOT NULL, PRIMARY KEY (studentID,title), FOREIGN KEY (studentID) REFERENCES student(studentID), FOREIGN KEY (title) REFERENCES course(title));");
            System.out.println("Database initialized successfully");
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }

    public static void resetDatabase() {
        Connection con = null;
        Statement stmt = null;

        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            stmt.executeUpdate("DROP TABLE medicalstaff cascade;");
            stmt.executeUpdate("DROP TABLE patient cascade;");
            stmt.executeUpdate("DROP TABLE user;");
            stmt.executeUpdate("DROP TABLE medicine cascade ;");
            stmt.executeUpdate("DROP TABLE hospitalTherapySpot cascade ;");
            stmt.executeUpdate("DROP TABLE folder cascade ;");
            stmt.executeUpdate("DROP TABLE visit;");
            stmt.executeUpdate("DROP TABLE test;");
            stmt.executeUpdate("DROP TABLE therapy;");
            stmt.executeUpdate("DROP TABLE surgery;");
            System.out.println("Database reset successfully");
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
    }
}
