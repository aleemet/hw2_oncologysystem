/**
 * @(#) Medicine.java
 */


public class Medicine {
    private String name;

    private String company;

    private String code;

    private Therapy therapy;

    public void setMedicineName(String name) {
        this.name=name;
    }

    public void setMedicineCompany(String company) {
        this.company=company;
    }

    public void setMedicineCode(String code) {
        this.code=code;
    }

    public String getName() {
        return name;
    }
    public String getCompany() {
        return company;
    }
    public String getCode() {
        return code;
    }


    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("'%s', '%s', '%s'", code, name, company);
    }
}
