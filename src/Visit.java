import java.time.LocalDate;

/**
 * @(#) Visit.java
 */


public class Visit {
    private LocalDate date;

    private String report;

    private String folder;

    private VisitType type;

    private String decision;

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public VisitType getType() {
        return type;
    }

    public void setType(VisitType type) {
        this.type = type;
    }

    public String getDecision() {
        return decision;
    }

    public void setDecision(String decision) {
        this.decision = decision;
    }

    @Override
    public String toString() {
        return String.format("'%s', '%s', '%s', '%s', '%s'", folder, date, report, type,decision);
    }
}
