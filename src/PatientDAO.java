import com.sun.xml.internal.bind.v2.model.core.ID;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * @(#) PatientDAO.java
 */


public class PatientDAO {
    public static boolean insertPatient(Patient patient) {
        Connection con = null;
        Statement stmt = null;
        int result = 0;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeUpdate("INSERT INTO PATIENT VALUES (" + patient.toString() + ")");
            con.commit();
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return result != 0;
    }

    public static boolean patientWithIDExists(String IDcode) {
        Connection con = null;
        Statement stmt = null;
        ResultSet result = null;
        boolean output = false;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection(
                    "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeQuery(
                    "SELECT * FROM PATIENT WHERE IDCODE = '" + IDcode + "'");
            while (result.next()) {
                output = true;
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return output;
    }

    public static Patient patientWithID(String IDcode){
        Connection con = null;
        Statement stmt = null;
        ResultSet result = null;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection(
                    "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeQuery(
                    "SELECT * FROM PATIENT WHERE IDCODE = '" + IDcode + "'");
            while (result.next()) {
                Patient patient = new Patient();
                patient.setIDcode(IDcode);
                patient.setName(result.getString("name"));
                patient.setSurname(result.getString("surname"));
                patient.setDob(LocalDate.parse(result.getString("dob"), DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                patient.setInsuranceCode(result.getString("insurancecode"));
                patient.setInsuranceType(InsuranceType.valueOf(result.getString("insurancetype")));
                patient.setInsuranceCompany(result.getString("insurancecompany"));
                return patient;
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return null;
    }

    public static List<Patient> patients(String name){
        Connection con = null;
        Statement stmt = null;
        ResultSet result = null;
        List<Patient> patients = new ArrayList<>();
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection(
                    "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeQuery(
                    "SELECT * FROM PATIENT WHERE NAME like '%" + name + "%' or SURNAME like '%" + name + "%'");
            while (result.next()) {
                Patient patient = new Patient();
                patient.setIDcode(result.getString("idcode"));
                patient.setName(result.getString("name"));
                patient.setSurname(result.getString("surname"));
                patient.setDob(LocalDate.parse(result.getString("dob"), DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                patient.setInsuranceCode(result.getString("insurancecode"));
                patient.setInsuranceType(InsuranceType.valueOf(result.getString("insurancetype")));
                patient.setInsuranceCompany(result.getString("insurancecompany"));
                patients.add(patient);
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return patients;
    }


}
