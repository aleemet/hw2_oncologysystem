/**
 * @(#) Boundary.java
 */


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Scanner;

public class Boundary {

    static User currentUser = null;

    public static void main(String[] args) {
        System.out.println("Do you want to initialize the database? (Y/N)");
        Scanner scan = new Scanner(System.in);
        if (scan.nextLine().equals("Y")) {
            DatabaseManager.resetDatabase();
            DatabaseManager.initializeDatabase();
        }
        while (true) {
            System.out.println("Select an option:");
            System.out.println("1. Log in");
            System.out.println("2. Register user");
            System.out.println("3. Add patient");
            System.out.println("4. Get patient by id");
            System.out.println("5. Get patients");
            System.out.println("7. Insert staff member");
            System.out.println("8. Get staff members");
            System.out.println("9. Insert  medicine");
            System.out.println("10. Get medicine");
            System.out.println("11. Open new folder");
            System.out.println("12. Delete folder");
            System.out.println("13. Copy previous tests");
            System.out.println("14. Write anamnesis");
            System.out.println("15. get patient folder");
            System.out.println("16. book test");
            System.out.println("6. Exit");
            int choice = new Integer(scan.nextLine());
            switch (choice) {
                case 1:
                    loginUser(scan);
                    break;
                case 2:
                    registerUser(scan);
                    break;
                case 3:
                    addPatient(scan);
                    break;
                case 4:
                    getPatientByID(scan);
                    break;
                case 5:
                    getPatientsByName(scan);
                    break;
                case 7:
                    insertStaff(scan);
                    break;
                case 8:
                    getMedicalStaff(scan);
                    break;
                case 9:
                    insertMedicine(scan);
                    break;
                case 10:
                    getMedicine(scan);
                    break;
                case 11:
                    openNewFolder(scan);
                    break;
                case 12:
                    deletePatientFolder(scan);
                    break;
                case 13:
                    copyPreviousTests(scan);
                    break;
                case 14:
                    writeAnamnesis(scan);
                    break;
                case 15:
                    getFolderByID(scan);
                    break;
                case 16:
                    bookTest(scan);
                    break;
                default:
                    choice = 6;
                    break;
            }
            if (choice == 6)
                break;
        }
        scan.close();
    }

    private static void bookTest(Scanner scan) {
        System.out.println("Insert folder id:");
        String folderID = scan.nextLine();
        System.out.println("insert date (dd.MM.yyyy):");
        LocalDate date = LocalDate.parse(scan.nextLine(), DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        System.out.println("insert test type(MRI, BloodTest, etc):");
        String testType = scan.nextLine();
        if (Controller.bookTest(folderID, date, testType))
            System.out.println("Test successfully booked!");
        else
            System.out.println("Test could not be booked!");
    }

    private static void copyPreviousTests(Scanner scan) {
        System.out.println("Insert folder ID:");
        String folderID = scan.nextLine();
        System.out.println("Insert test results catalogue code:");
        String result = scan.nextLine();
        if (Controller.copyPrevooisTests(folderID, result))
            System.out.println("Folder " + folderID + " tests updated");
        else System.out.println("Failed at updating folder tests");
    }

    private static void writeAnamnesis(Scanner scan) {
        System.out.println("Insert folder ID:");
        String folderID = scan.nextLine();
        System.out.println("Insert test results catalogue code:");
        String result = scan.nextLine();
        if (Controller.writeAnamnesis(folderID, result))
            System.out.println("Folder " + folderID + " tests updated");
        else
            System.out.println("Failed at updating folder anamnesis");
    }

    private static void openNewFolder(Scanner scan) {
        System.out.println("Insert patient ID:");
        String patientID = scan.nextLine();
        System.out.println("Insert follower's professional ID:");
        String followerID = scan.nextLine();
        System.out.println("Insert first visit date (dd.MM.yyyy):");
        LocalDate visit = LocalDate.parse(scan.nextLine(), DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        int result = Controller.openNewFolder(patientID, followerID, visit);
        switch (result){
            case 0:
                System.out.println("Folder opened!");
                break;
            case -1:
                System.out.println("Folder already open!");
            case -2:
                System.out.println("Patient with id " + patientID + " doesn't exist");
                break;
            case -3:
                System.out.println("Medical staff with id " + followerID + " doesn't exist");
                break;
            case -4:
                System.out.println("Failed to create first visit");
                break;
            case -5:
                System.out.println("Failed to open folder");
                break;
        }
    }

    private static void getFolderByID(Scanner scan) {
        System.out.println("Insert patient ID: ");
        String idcode = scan.nextLine();
        Folder folder = Controller.getPatientFolder(idcode);
        if (folder == null)
            System.out.println("No folder with that ID exists");
        else
            System.out.println("Patient: " + folder.toString());
    }

    private static void deletePatientFolder(Scanner scan) {
        System.out.println("Insert patient ID:");
        String patientID = scan.nextLine();
        if (Controller.deletePatientFolder(patientID))
            System.out.println("Patient folder deleted");
        else
            System.out.println("Error while deleting folder");
    }

    private static void getMedicine(Scanner scan) {
        System.out.println("Insert medicine name or part of it:");
        String med = scan.nextLine();
        List<Medicine> medicines = Controller.getMedicine(med);
        System.out.println("Medicine with \"" + med + "\" in the name:");
        for (Medicine m : medicines) {
            System.out.println(m.toString());
        }
    }

    private static void getMedicalStaff(Scanner scan) {
        System.out.println("Insert professional id or part of it:");
        String med = scan.nextLine();
        List<MedicalStaff> staff = Controller.getStaff(med);
        System.out.println("Staff members with \"" + med + "\" in the professional ID:");
        for (MedicalStaff m : staff) {
            System.out.println(m.toString());
        }
    }

    private static void insertStaff(Scanner scan) {
        System.out.println("Insert name:");
        String sname = scan.nextLine();
        System.out.println("Insert surname:");
        String ssname = scan.nextLine();
        System.out.println("Insert type:");
        String stype = scan.nextLine();
        System.out.println("Insert professionalID:");
        String sproid = scan.nextLine();
        System.out.println("Insert occupation:");
        String socc = scan.nextLine();

        boolean staffInserted = Controller.insertStaffMember(sname, ssname, stype, socc, sproid);
        if(staffInserted)
            System.out.println("Staff member insterted!");
    }

    private static void insertMedicine(Scanner scan) {
        System.out.println("Insert name:");
        String mname = scan.nextLine();
        System.out.println("Insert company:");
        String company = scan.nextLine();
        System.out.println("Insert code:");
        String code = scan.nextLine();

        boolean medInserted = Controller.insertMedicine(mname, code, company);
        if(medInserted)
            System.out.println("Medicine insterted!");
    }

    private static void getPatientsByName(Scanner scan) {
        System.out.println("Insert patient name:");
        String pname = scan.nextLine();
        List<Patient> patients = Controller.getPatients(pname);
        System.out.println("Patients with name " + pname + ":");
        for (Patient p : patients) {
            System.out.println(p.toString());
        }
    }

    private static void getPatientByID(Scanner scan) {
        System.out.println("Insert patient ID: ");
        String idcode = scan.nextLine();
        Patient patient = Controller.getPatientInfo(idcode);
        if (patient == null)
            System.out.println("No patient with that ID exists");
        else
            System.out.println("Patient: " + patient.toString());
    }

    private static void addPatient(Scanner scan) {
        System.out.println("Insert patient name: ");
        String name = scan.nextLine();
        System.out.println("Insert patient surname: ");
        String sname = scan.nextLine();
        System.out.println("Insert patient date of birth (dd.MM.YYY): ");
        LocalDate dob = LocalDate.parse(scan.nextLine(), DateTimeFormatter.ofPattern("dd.MM.yyyy"));
        System.out.println("Insert patient ID:");
        String id = scan.nextLine();
        System.out.println("Insert patient insurance type: ");
        InsuranceType itype = InsuranceType.valueOf(scan.nextLine().toUpperCase());
        String icode;
        String icomp;
        if (itype == InsuranceType.NATIONAL) {
            icode = sname + id;
            icomp = "";
        } else {
            System.out.println("Insert patient private insurance code:");
            icode = scan.nextLine();
            System.out.println("Insert patient insurance company:");
            icomp = scan.nextLine();
        }
        boolean added = Controller.addPatient(name, sname, dob, id, itype, icode, icomp);
        if (added)
            System.out.println("Patient registered!");
        else
            System.out.println("Failed to add patient");
    }

    private static void registerUser(Scanner scan) {
        System.out.println("Insert username:");
        String un = scan.nextLine();
        System.out.println("Insert password:");
        String pw = scan.nextLine();
        System.out.println("Insert role:");
        String role = scan.nextLine();

        boolean registered = Controller.registerUser(un, pw, role);
        if (registered)
            System.out.println("User registered!");
        else
            System.out.println("Failed to add user!");
    }

    private static void loginUser(Scanner scan) {
        System.out.println("Insert username:");
        String username = scan.nextLine();
        System.out.println("Insert password:");
        String password = scan.nextLine();
        User loggedIn = Controller.login(username, password);
        if (loggedIn == null)
            System.out.println("User not found or wrong password");
        else {
            System.out.println("Logged in!");
            currentUser = loggedIn;
        }
    }
}
