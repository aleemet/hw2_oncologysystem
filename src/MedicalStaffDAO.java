import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @(#) MedicalStaffDAO.java
 */


public class MedicalStaffDAO {
    public static boolean staffWithIDExists(String professionalID) {
        Connection con = null;
        Statement stmt = null;
        ResultSet result = null;
        boolean output = false;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection(
                    "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeQuery(
                    "SELECT * FROM MEDICALSTAFF WHERE PROFESSIONALID = '"+professionalID+"'");
            while(result.next()){
                output= true;
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }

        return output;
    }

    public static boolean insertStaff(MedicalStaff medicalStaff) {
        Connection con = null;
        Statement stmt = null;
        int result = 0;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection( "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeUpdate("INSERT INTO MEDICALSTAFF VALUES ("+medicalStaff.toString()+")");
            con.commit();
        }catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return result != 0;
    }


    public static List<MedicalStaff> staffExists(String professionalID) {
        Connection con = null;
        Statement stmt = null;
        ResultSet result = null;
        List<MedicalStaff> staff = new ArrayList<>();
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection(
                    "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeQuery(
                    "SELECT * FROM MEDICALSTAFF WHERE professionalID = '%"+professionalID+"%'");
            while(result.next()){
                MedicalStaff member = new MedicalStaff();
                member.setName(result.getString("name"));
                member.setProfessionalID(result.getString("professionalID"));
                member.setSurname(result.getString("surname"));
                member.setType(result.getString("type"));
                member.setOccupation(result.getString("occupation"));
                staff.add(member);
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return staff;
    }

    public static List<MedicalStaff> getStaff(String name){
        Connection con = null;
        Statement stmt = null;
        ResultSet result = null;
        List<MedicalStaff> staff = new ArrayList<>();
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection(
                    "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeQuery(
                    "SELECT * FROM MEDICALSTAFF WHERE PROFESSIONALID like '%" + name + "%' ");
            while (result.next()) {
                MedicalStaff member = new MedicalStaff();
                member.setName(result.getString("name"));
                member.setProfessionalID(result.getString("professionalID"));
                member.setSurname(result.getString("surname"));
                member.setType(result.getString("type"));
                member.setOccupation(result.getString("occupation"));
                staff.add(member);
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return staff;
    }
}
