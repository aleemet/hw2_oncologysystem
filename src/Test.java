import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @(#) Test.java
 */


public class Test {

    private String folderID;
    private LocalDate date;
    private TestType type;

    public String getFolderID() {
        return folderID;
    }

    public void setFolderID(String folderID) {
        this.folderID = folderID;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public TestType getType() {
        return type;
    }

    public void setType(String type) {
        this.type = TestType.valueOf(type.toUpperCase());
    }

    @Override
    public String toString() {
        return String.format("'%s', '%s', '%s'", folderID, date, type);
    }
}
