import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @(#) FolderDAO.java
 */


public class FolderDAO {
    public static boolean insertNewFolder(Folder patientFolder) {
        Connection con = null;
        Statement stmt = null;
        int result = 0;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeUpdate("INSERT INTO FOLDER VALUES (" + patientFolder.toString() + ")");
            con.commit();
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return result != 0;
    }

    public static boolean folderWithPatientExists(String patientID) {
        Connection con = null;
        Statement stmt = null;
        ResultSet result = null;
        boolean output = false;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection(
                    "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeQuery(
                    "SELECT * FROM FOLDER WHERE PATIENTID = '" + patientID + "'");
            while (result.next()) {
                output = true;
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return output;
    }

    public static boolean deletePatientFolder(String IDcode) {
        Connection con = null;
        Statement stmt = null;
        int result = 0;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeUpdate("DELETE from FOLDER where PATIENTID = (" + IDcode + ")");
            con.commit();
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return result != 0;
    }

    public static Folder getPatientFolder(String patientID) {
        Connection con = null;
        Statement stmt = null;
        ResultSet result = null;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection(
                    "jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeQuery(
                    "SELECT * FROM FOLDER WHERE PATIENTID = '" + patientID + "'");
            while (result.next()) {
                Folder folder = new Folder();
                folder.setPatientID(result.getString("patientid"));
                folder.setFollowerID(result.getString("follower"));
                folder.setTestHistory(result.getString("testhistory"));
                folder.setAnamnesis(result.getString("anamnesis"));

                return folder;
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return null;
    }



    public void patientFolderWithIDExists(String patientID) {

    }

    public static boolean updateFolderTests(String folderID, String tests) {
        Connection con = null;
        Statement stmt = null;
        int result = 0;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeUpdate("UPDATE FOLDER set TESTHISTORY = '" + folderID + "' where PATIENTID = (" + folderID + ")");
            con.commit();
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return result != 0;
    }

    public static boolean updateFolderAnamnesis(String folderID, String anamnesis) {
        Connection con = null;
        Statement stmt = null;
        int result = 0;
        try {
            Class.forName("org.hsqldb.jdbc.JDBCDriver");
            con = DriverManager.getConnection("jdbc:hsqldb:hsql://localhost/hospitaldb", "SA", "");
            stmt = con.createStatement();
            result = stmt.executeUpdate("UPDATE FOLDER set ANAMNESIS = '" + folderID + "' where PATIENTID = (" + folderID + ")");
            con.commit();
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return result != 0;
    }


}
