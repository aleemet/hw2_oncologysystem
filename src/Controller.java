import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @(#) Controller.java
 */


public class Controller {
    public static boolean registerUser(String userName, String password, String role) {
        if (UserDAO.userWithNameExists(userName))
            return true;
        else {
            User user = new User();
            user.setUsername(userName);
            user.setPassword(password);
            user.setRole(role);
            return UserDAO.insertUser(user);
        }
    }

    public static boolean insertStaffMember(String name, String surname, String type, String occupation, String proid) {
        if (UserDAO.userWithNameExists(proid))
            return true;
        else {
            MedicalStaff member = new MedicalStaff();
            member.setOccupation(occupation);
            member.setType(type);
            member.setSurname(surname);
            member.setProfessionalID(proid);
            member.setName(name);
            return MedicalStaffDAO.insertStaff(member);
        }
    }
    public static boolean insertMedicine(String name, String code, String company) {
        if (MedicineDAO.medicineWithNameExists(name))
            return true;
        else {
            Medicine med = new Medicine();
            med.setMedicineCompany(company);
            med.setMedicineCode(code);
            med.setName(name);
            return MedicineDAO.insertMedicine(med);
        }
    }
    public static List<Medicine> getMedicine(String str){
        return MedicineDAO.getMeds(str);
    }

    public static List<MedicalStaff> getStaff(String str){
        return MedicalStaffDAO.getStaff(str);
    }

    public static boolean addPatient(String name, String surname, LocalDate dob, String IDcode, InsuranceType insuranceType, String insuranceCode, String insuranceCompany) {
        if (PatientDAO.patientWithIDExists(IDcode))
            return true;
        else {
            Patient patient = new Patient();
            patient.setIDcode(IDcode);
            patient.setName(name);
            patient.setSurname(surname);
            patient.setDob(dob);
            patient.setInsuranceType(insuranceType);
            patient.setInsuranceCode(insuranceCode);
            patient.setInsuranceCompany(insuranceCompany);

            return PatientDAO.insertPatient(patient);
        }
    }

    public static int openNewFolder(String patientID, String followerID, LocalDate firstVisitDate) {

        if (FolderDAO.folderWithPatientExists(patientID))
            return -1;
        if (!PatientDAO.patientWithIDExists(patientID))
            return -2;
        if (!MedicalStaffDAO.staffWithIDExists(followerID))
            return -3;

        Folder folder = new Folder();
        folder.setPatientID(patientID);
        folder.setFollowerID(followerID);
        boolean inserted = FolderDAO.insertNewFolder(folder);



        if (!bookFirstVisit(firstVisitDate, patientID))
            return -4;

        if (inserted)
            return 0;
        return -5;
    }

    public static User login(String userName, String password) {
        List<User> users;
        if (UserDAO.userWithNameExists(userName))
            users = UserDAO.userExists(userName, password);
        else
            return null;
        for (User user : users) {
            if (user.getPassword().equals(password))
                return user;
        }
        return null;
    }

    public static List<Patient> getPatients(String patientName) {
        return PatientDAO.patients(patientName);
    }

    public static boolean deletePatientFolder(String IDcode) {
        return FolderDAO.deletePatientFolder(IDcode);
    }

    public static Patient getPatientInfo(String patientID) {
        return PatientDAO.patientWithID(patientID);
    }

    public static boolean copyPrevooisTests(String tests, String patientID) {
        return FolderDAO.updateFolderTests(patientID, tests);
    }

    public static Folder getPatientFolder(String patientID) {
        return FolderDAO.getPatientFolder(patientID);
    }

    public static boolean bookTest(String patientID, LocalDate date, String testType) {
        if (TestDAO.testExists(patientID, date))
            return true;
        Test test = new Test();
        test.setFolderID(patientID);
        test.setDate(date);
        test.setType(testType);
        return TestDAO.insertTest(test);
    }

    public static boolean bookFirstVisit(LocalDate date, String patientID) {
        if (VisitDAO.firstVisitExists(patientID))
            return true;
        Visit visit = new Visit();
        visit.setFolder(patientID);
        visit.setDate(date);
        visit.setType(VisitType.FIRSTVISIT);
        return VisitDAO.insertVisit(visit);

    }

    public static boolean bookFolloupVisit(LocalDate date, String patientID) {
        if (VisitDAO.followupVisitExists(date, patientID))
            return true;
        Visit visit = new Visit();
        visit.setFolder(patientID);
        visit.setDate(date);
        visit.setType(VisitType.FOLLOWUPVISIT);
        return VisitDAO.insertVisit(visit);
    }

    public static void bookSurgery(LocalDateTime date, String patientID, MedicalStaff surgeon) {

    }

    public static void defineSurgeryTeam(String team, Surgery surgery) {

    }

    public static void getTests(String patientID) {

    }

    public static boolean writeAnamnesis(String catalogueCode, String patientID) {
        return FolderDAO.updateFolderAnamnesis(patientID, catalogueCode);
    }

    public static void writeReport(String reportCode, String patientID) {

    }

    public static void assignTherapy(LocalDateTime dateStart, LocalDateTime dateEnd, TherapyType type, Medicine medicine, String posology) {

    }


}
