/**
 * @(#) Folder.java
 */


public class Folder {

    String patientID;
    String followerID;
    String testHistory;
    String anamnesis;

    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    public String getFollowerID() {
        return followerID;
    }

    public void setFollowerID(String followerID) {
        this.followerID = followerID;
    }

    public String getTestHistory() {
        return testHistory;
    }

    public void setTestHistory(String testHistory) {
        this.testHistory = testHistory;
    }

    public String getAnamnesis() {
        return anamnesis;
    }

    public void setAnamnesis(String anamnesis) {
        this.anamnesis = anamnesis;
    }

    @Override
    public String toString() {
        return String.format("'%s', '%s', '%s', '%s'", patientID, followerID, testHistory, anamnesis);
    }
}
