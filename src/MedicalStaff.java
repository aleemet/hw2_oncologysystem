/**
 * @(#) MedicalStaff.java
 */


public class MedicalStaff {
    private Surgery surgery;

    private Oncology work;

    private String name;

    private String surname;

    private String professionalID;
    private StaffOccupation occupation;
    private String type;

    public String getName() {
        return name;
    }
    public String getSurname() {
        return surname;
    }
    public String getProfessionalID() {
        return professionalID;
    }
    public StaffOccupation getOccupation() {
        return occupation;
    }
    public String getType() {
        return type;
    }


    public void setName(String name) {
        this.name = name;
    }
    public void setOccupation(String occupation){ this.occupation = StaffOccupation.valueOf(occupation.toUpperCase());}
    public void setType(String type) {
        this.type = type;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setProfessionalID(String professionalID) {
        this.professionalID = professionalID;
    }

    @Override
    public String toString() {
        return String.format("'%s', '%s', '%s', '%s', '%S'", professionalID, name, surname, type, occupation);
    }
}


